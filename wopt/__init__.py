#!/usr/bin/env python3
# -*- coding: utf-8 -*-
with open(__path__[0] + "/../setup.py", "r") as f:
    for l in f.readlines():
        if "version=" in l:
            __version__ = l.split('"')[1]
            break
