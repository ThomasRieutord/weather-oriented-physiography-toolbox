#!/usr/bin/bash
#
# Create a conda environment with the GDAL package correctly installed
#
# Usage:
#   1. Check that the variable GDAL_VERSION is set to the version of GDAL you want
#   2. Run ./build_condaenv.sh $(conda info --base)
# 
export GDAL_VERSION=3.4.1
CONDAPATH="$1/etc/profile.d/conda.sh"

# 1. Install the GDAL Linux package (without being root)
#------------------------------------------------------
if [ $GDAL_VERSION == $(gdal-config --version) ]
then
    echo "Installing GDAL version $GDAL_VERSION..."
    cd tmp
    wget https://github.com/OSGeo/gdal/releases/download/v$GDAL_VERSION/gdal-$GDAL_VERSION.tar.gz
    tar xzf gdal-$GDAL_VERSION.tar.gz
    cd gdal-$GDAL_VERSION
    
    ./autogen.sh 
    ./configure --prefix=~/.local --with-openjpeg --without-jasper --with-python=python3 
    make install
    # This may take a while (few ten minutes)...
    
    cd ..
    rm -rf gdal-$GDAL_VERSION gdal-$GDAL_VERSION.tar.gz
    cd ..
else
    echo "GDAL version $GDAL_VERSION already installed"
fi

gdal-config --version
# Should return $GDAL_VERSION

# 2. Install the Python package in a Conda environment
#-----------------------------------------------------

echo "
name: wopt
channels:
- conda-forge
- defaults
dependencies:
- python=3.9.*
- cython
- numpy
- pandas
- ipython
- matplotlib
- netCDF4
- libspatialindex
- geopandas
- pyproj
- pyrosm
- rasterio
- libgdal=$GDAL_VERSION
- gdal=$GDAL_VERSION
- pip
- pip:
  - GDAL==$GDAL_VERSION
" > condaenv.yml

source $CONDAPATH
conda env create -f condaenv.yml -v
rm condaenv.yml
conda activate wopt
pip install -e .

python -c "from osgeo import gdal"
# Should run fast and return nothing
python -c "import wopt; print(wopt.__version__)"
# Should run fast and return the WOPT version number

