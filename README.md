Weather-oriented physiography toolbox (WOPT)
===================

This package contains useful codes to access and generate physiographic maps that would be useful for numerical weather prediction (NWP) models.


Installation
------------
The installation procedure assumes that you are working on a Linux machine without specific installation for Python packages and that you have no super-user privileges.
The installation uses [Conda](https://docs.conda.io/projects/conda/en/latest/index.html) package manager.
Please install it before starting the installation (sudo rights not needed).

To install the package, run the following commands:
```
./installer.sh $(conda info --base)
```

Contents
--------
The package is composed of the following directories:
  * `wopt` -> contains only classes and functions of the package
  * `examples` -> contains only executable programs using the package
  * `tests` -> contains unitary tests
  * `tmp` -> contains temporary files
  * `local` -> contains local files, not meant be shared

See also
--------

### Related repositories

  * https://github.com/gbessardon/Cropecosg
  * https://github.com/gbessardon/Create_plots
  * https://github.com/gbessardon/Land_Cover_comparison

### Related publications

  * The Ulmas-Walsh land cover map: https://asr.copernicus.org/articles/18/65/2021/
