import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="wopt",
    version="0.0",
    author="",
    author_email="",
    description="",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="",
    packages=setuptools.find_packages(),
    classifiers=(
        "Environment :: Console"
        "Programming Language :: Python :: 3",
        "Operating System :: Linux",
        "Development Status :: 2 - Pre-Alpha",
    ),
)
